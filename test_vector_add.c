#include <assert.h>
#include "vector.h"

/** A simple test framework for vector library
* we will be improving on this later
*/
int main(void) {
    /** xvec and yvec will be inputs to our vector arithmetic routines
    * zvec will take the return value
    */
    int xvec[SIZ]={1,2,3};
    int yvec[SIZ]={5,0,2};
    int zvec[SIZ];
    add_vectors(xvec,yvec,zvec);
    /** We want to check each element of the returned vector
    */
    assert(5==zvec[2]);
    assert(5==zvec[1]);
    assert(5==zvec[2]);
    /** If the asserts worked, there wasn't an error so return 0
    */
    return 0;
}

#include "vector.h"

/** A simple fixed size vector addition routine
* Add each element of x to corresponding element of y, storing answer in z
* It is the calling codes responsibility to ensure they are the right size and that they have been declared.
* We return an error code (0 in this case showing no error), but will add the program logic to handle actual errors later
*/
int dot_product(int x[], int y[]) {
/** res <- a local variable to hold the result as we calculate it
*/
    int res =0;
    for (int i=0;i<SIZ;i++)
      res=res + x[i]*y[i];
    return res;
}

