# cs1pc20_portfolio



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://csgitlab.reading.ac.uk/bz838503/cs1pc20_portfolio.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://csgitlab.reading.ac.uk/bz838503/cs1pc20_portfolio/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

Last login: Fri Dec  2 03:25:00 on ttys001
mingclaudia@MingdeMacBook-Air ~ % cd ~
mingclaudia@MingdeMacBook-Air ~ % cd portfolio
cd: no such file or directory: portfolio
mingclaudia@MingdeMacBook-Air ~ % mkdir week 3
mingclaudia@MingdeMacBook-Air ~ % mkdir week 3/greeting
mkdir: week: File exists
mingclaudia@MingdeMacBook-Air ~ % cd week 3/greeting
cd: string not in pwd: week
mingclaudia@MingdeMacBook-Air ~ % git branch
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % git branch greeting
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % git switch greeting
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % nano greeting.c
mingclaudia@MingdeMacBook-Air ~ % gcc -Wall -pedantic -c greeting.c -o greeting.o
mingclaudia@MingdeMacBook-Air ~ % nano test_result.c
mingclaudia@MingdeMacBook-Air ~ % int greet(void);
zsh: unknown file attribute: v
mingclaudia@MingdeMacBook-Air ~ % nano greeting.h
mingclaudia@MingdeMacBook-Air ~ % echo greeting.o >> ~/portfolio/.gitignore
zsh: no such file or directory: /Users/mingclaudia/portfolio/.gitignore
mingclaudia@MingdeMacBook-Air ~ % echo libgreet.a >> ~/portfolio/.gitignore
zsh: no such file or directory: /Users/mingclaudia/portfolio/.gitignore
mingclaudia@MingdeMacBook-Air ~ % ar rv libgreet.a greeting.o
ar: creating archive libgreet.a
a - greeting.o
mingclaudia@MingdeMacBook-Air ~ % gcc test_result.c -o test1 -L. -lgreet -I.
mingclaudia@MingdeMacBook-Air ~ % ./test1
Hello world!
mingclaudia@MingdeMacBook-Air ~ % git add -A
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % git commit -m "greeting library and greeting test program"
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % git push
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % pause
zsh: command not found: pause
mingclaudia@MingdeMacBook-Air ~ % git switch master
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % git branch vectors
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % git switch vectors
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air ~ % cd ~/portfolio/week3
cd: no such file or directory: /Users/mingclaudia/portfolio/week3
mingclaudia@MingdeMacBook-Air ~ % mkdir vectors
mingclaudia@MingdeMacBook-Air ~ % cd vectors
mingclaudia@MingdeMacBook-Air vectors % nano vector.h
mingclaudia@MingdeMacBook-Air vectors % nano test_vector_add.c
mingclaudia@MingdeMacBook-Air vectors % nano test_vector_add.c
mingclaudia@MingdeMacBook-Air vectors % gcc -Wall -pedantic -c vector.c -o vector.o
clang: error: no such file or directory: 'vector.c'
clang: error: no input files
mingclaudia@MingdeMacBook-Air vectors % nano vector.c
mingclaudia@MingdeMacBook-Air vectors % 
mingclaudia@MingdeMacBook-Air vectors % gcc -Wall -pedantic -c vector.c -o vector.o
mingclaudia@MingdeMacBook-Air vectors % ar rv libvector.a vector.o
ar: creating archive libvector.a
a - vector.o
mingclaudia@MingdeMacBook-Air vectors % gcc test_vector_add.c -o test_vector_addl -L. -lvector -I.
mingclaudia@MingdeMacBook-Air vectors % ./test_vector_addl
mingclaudia@MingdeMacBook-Air vectors % git add -A
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air vectors % git comit -m "code to add two vectors of fixed size"
git: 'comit' is not a git command. See 'git --help'.

The most similar command is
	commit
mingclaudia@MingdeMacBook-Air vectors % git push
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air vectors % nano test_vector_add.c        
mingclaudia@MingdeMacBook-Air vectors % nano vector.h
mingclaudia@MingdeMacBook-Air vectors % 
mingclaudia@MingdeMacBook-Air vectors % nano vector.c
mingclaudia@MingdeMacBook-Air vectors % nano test_vector_dot_product.c
mingclaudia@MingdeMacBook-Air vectors % gcc -Wall -pedantic -c vector.c -o vector.o
vector.c:25:5: error: redefinition of 'add_vectors'
int add_vectors(int x[], int y[], int z[]) {
    ^
vector.c:11:5: note: previous definition is here
int add_vectors(int x[], int y[], int z[]) {
    ^
vector.c:39:5: error: conflicting types for 'dot_product'
int dot_product(int x[], int y[]) {
    ^
./vector.h:3:5: note: previous declaration is here
int dot_product(int x[], int y[], int z[]);
    ^
2 errors generated.
mingclaudia@MingdeMacBook-Air vectors % ar rv libvector.a vector.o
ar: vector.o: No such file or directory
mingclaudia@MingdeMacBook-Air vectors % gcc test_vector_dot_product.c -o test_vector_dot_product1 -L. -lvector -I.
test_vector_dot_product.c:12:31: error: too few arguments to function call, expected 3, have 2
  result=dot_product(xvec,yvec);
         ~~~~~~~~~~~          ^
./vector.h:3:5: note: 'dot_product' declared here
int dot_product(int x[], int y[], int z[]);
    ^
1 error generated.
mingclaudia@MingdeMacBook-Air vectors % ./test_vector_dot_product1
zsh: no such file or directory: ./test_vector_dot_product1
mingclaudia@MingdeMacBook-Air vectors % git add -A
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air vectors % git commit -m "code to calculate dot product of two vectors of fixed size"
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air vectors % git push
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air vectors % history > my_work_log.md
mingclaudia@MingdeMacBook-Air vectors % portfolio/week3/c-programs.md
zsh: no such file or directory: portfolio/week3/c-programs.md
mingclaudia@MingdeMacBook-Air vectors % cd existing_repo
git remote add origin https://csgitlab.reading.ac.uk/bz838503/cs1pc20_portfolio.git
git branch -M main
git push -uf origin main

cd: no such file or directory: existing_repo
fatal: not a git repository (or any of the parent directories): .git
fatal: not a git repository (or any of the parent directories): .git
fatal: not a git repository (or any of the parent directories): .git
mingclaudia@MingdeMacBook-Air vectors % 

